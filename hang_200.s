;		DSPic flash a LED
;       .equ __30F2010, 1
        .include "p24fj64ga002.inc"

        .global hang_200, hang_50, hang_400

;..............................................................................
;Uninitialized variables in Near data memory (Lower 8Kb of RAM)
;..............................................................................

          .section .nbss, bss, near
loopc:		.space		2
loop2:		.space		2
        
;..............................................................................
;Subroutine: Delay 200 us
;..............................................................................
.text
hang_around:
		MOV			#1064,W1
		MOV			W1,loopc
hang_ax:
		DEC			loopc
		BRA			NZ,hang_ax
		RETURN

;..............................................................................
;Subroutine: Delay 200 ms
;..............................................................................
hang_200:							; 200.06 ms @ 64
		MOV			#1000,W0
		MOV			W0,loop2
hang_2x:
		CALL		hang_around
		DEC			loop2
		BRA			NZ,hang_2x
		RETURN

;..............................................................................
;Subroutine: Delay 50 ms
;..............................................................................
hang_50:
		MOV			#250,W0
		MOV			W0,loop2
hang_5x:
		CALL		hang_around
		DEC			loop2
		BRA			NZ,hang_5x
		RETURN

;..............................................................................
;Subroutine: Delay 400 ms
;..............................................................................
hang_400:
		MOV			#2000,W0
		MOV			W0,loop2
hang_4x:
		CALL		hang_around
		DEC			loop2
		BRA			NZ,hang_4x
		RETURN

.end

