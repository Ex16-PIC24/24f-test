#include <p30f4012.h>
#include <stdio.h>

#define XTFREQ          7372800         		//On-board Crystal frequency
#define PLLMODE         16               		//On-chip PLL setting
#define FCY             XTFREQ*PLLMODE/4        //Instruction Cycle Frequency
#define BAUDRATE         9600       
#define BRGVAL          ((FCY/BAUDRATE)/16)-1 	// Value for the baud rate generator

int main( void )
{
	// UART setup
	TRISF = 0xC;
	U1BRG  = BRGVAL;
	U1MODE = 0x8000; 			// Reset UART to 8-n-1, alt pins, and enable 
	U1STA  = 0x0440; 			// Reset status register and enable TX & RX


	printf("\r\nHello, world!\r\n");
	
	while ( 1 )
		;
		
}

