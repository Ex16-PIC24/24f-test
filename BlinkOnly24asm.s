;		DSPic flash a LED
       .equ __30F2010, 1
        .include "p24fj64ga002.inc"

;..............................................................................
;Configuration bits:
;..............................................................................

;        config __FOSC, CSW_FSCM_OFF & XT_PLL16    ;Turn off clock switching and
;                                            ;fail-safe clock monitoring and
;                                            ;use the External Clock as the
;                                            ;system clock
;
;        config __FWDT, WDT_OFF              ;Turn off Watchdog Timer
;
;        config __FBORPOR, PBOR_OFF & BORV_27 & PWRT_16 & MCLR_EN
;                                            ;Set Brown-out Reset voltage and
;                                            ;and set Power-up Timer to 16msecs
;                                            
;        config __FGS, CODE_PROT_OFF         ;Set Code Protection Off for the 
;                                            ;General Segment
;
;;        config  __DEBUG
;
;..............................................................................
;Global Declarations:
;..............................................................................

        .global    __reset          ;The label for the first line of code. 
        .global     __ICD2RAM
;        .global     count

;        .section   .icd
;__ICD2RAM:        .space      0x24
        .section    .bss
count:  .space     2

;..............................................................................
;Code Section in Program Memory
;..............................................................................

.text								;Start of Code section
__reset:
        mov     #__SP_init, W15		;Initalize the Stack Pointer
        mov     #__SPLIM_init, W0	;Initialize the Stack Pointer Limit Register
        mov     W0, SPLIM
        nop							;Add nop to follow SPLIM initialization
        
		call	port_init		    ; Initialize PORTC<14> as output

loop:

    nop
    nop
        mov     #100,W0
        mov     W0,count
loop5:
		call	hang_50 		    ; Wait 1/20th second
        call    toggle_led          ; Toggle the LED condition
        dec     count
		bra		NZ,loop5		    ; Do it again

        mov     #25,W0
        mov     W0,count
loop2:
		call	hang_200 		    ; Wait 1/5th second
        call    toggle_led          ; Toggle the LED condition
        dec     count
		bra		NZ,loop2		    ; Do it again

        mov     #13,W0
        mov     W0,count
loop4:
		call	hang_400 		    ; Wait 1/2.5th second
        call    toggle_led          ; Toggle the LED condition
        dec     count
		bra		NZ,loop4		    ; Do it again

        goto    loop

.end                                ;End of program code in this file

