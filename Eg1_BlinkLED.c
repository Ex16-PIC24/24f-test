/**********************************************************************************
* � 2007 Microchip Technology Inc.
*
* FileName:        Eg1_BlinkLED.c
* Dependencies:    p30f2010.h
* Processor:       dsPIC30F2010
* Compiler:        MPLAB� C30 v3
*
* SOFTWARE LICENSE AGREEMENT:
* Microchip Technology Incorporated ("Microchip") retains all ownership and 
* intellectual property rights in the code accompanying this message and in all 
* derivatives hereto.  You may use this code, and any derivatives created by 
* any person or entity by or on your behalf, exclusively with Microchip,s 
* proprietary products.  Your acceptance and/or use of this code constitutes 
* agreement to the terms and conditions of this notice.
*
* CODE ACCOMPANYING THIS MESSAGE IS SUPPLIED BY MICROCHIP "AS IS".  NO 
* WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED 
* TO, IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A 
* PARTICULAR PURPOSE APPLY TO THIS CODE, ITS INTERACTION WITH MICROCHIP,S 
* PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
*
* YOU ACKNOWLEDGE AND AGREE THAT, IN NO EVENT, SHALL MICROCHIP BE LIABLE, WHETHER 
* IN CONTRACT, WARRANTY, TORT (INCLUDING NEGLIGENCE OR BREACH OF STATUTORY DUTY), 
* STRICT LIABILITY, INDEMNITY, CONTRIBUTION, OR OTHERWISE, FOR ANY INDIRECT, SPECIAL, 
* PUNITIVE, EXEMPLARY, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, FOR COST OR EXPENSE OF 
* ANY KIND WHATSOEVER RELATED TO THE CODE, HOWSOEVER CAUSED, EVEN IF MICROCHIP HAS BEEN 
* ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT 
* ALLOWABLE BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO 
* THIS CODE, SHALL NOT EXCEED THE PRICE YOU PAID DIRECTLY TO MICROCHIP SPECIFICALLY TO 
* HAVE THIS CODE DEVELOPED.
*
* You agree that you are solely responsible for testing the code and 
* determining its suitability.  Microchip has no obligation to modify, test, 
* certify, or support the code.
*
********************************************************************************************
*
* Description:  In this example program characters pressed on the keyboard are sent to
* the device via usb. The device replicates the character back to the PC enclosed in quotes. 
* Also this program blinks all four LEDs at 250ms by using timer 1. 
*
*******************************************************************************************/ 

#include "p30F2010.h"

// the above include path may be different for each user.  If a compile
// time error appears then check the path for the file above and edit
// the include statement above.

#define XTFREQ          7372800         		//On-board Crystal frequency
#define PLLMODE         16               		//On-chip PLL setting
#define FCY             XTFREQ*PLLMODE/4        //Instruction Cycle Frequency

#define BAUDRATE         9600       
#define BRGVAL          ((FCY/BAUDRATE)/16)-1 

// ***********************************************
//#define LATE LATB
//#define TRISE TRISB
// ***********************************************

int main(void)
{

	ADPCFG = 0xFF;			//Make analog pins digital 

	LATE = 0x0;
	TRISE = 0x0;			//Configure LED pins as output

	TMR1 = 0;				// clear timer 1
	PR1 = 0x7270;			// interrupt every 250ms
	IFS0bits.T1IF = 0;		// clr interrupt flag
	IEC0bits.T1IE = 1;		// set interrupt enable bit
	T1CON = 0x8030;			// Fosc/4, 1:256 prescale, start TMR1


	TRISF = 0xC;
	U1BRG  = BRGVAL;
	U1MODE = 0x8000; 		// Reset UART to 8-n-1, alt pins, and enable 
	U1STA  = 0x0440; 		// Reset status register and enable TX & RX


	_U1RXIF=0;					// Clear UART RX Interrupt Flag


	while(1)
	{
		int a;	

		while (_U1RXIF==0);			// Wait and receive one Character
		a = U1RXREG;

		while(!U1STAbits.TRMT);		// Echo back received character with quotes
		U1TXREG = '"';
	
		while(!U1STAbits.TRMT);
		U1TXREG = a;
	
		while(!U1STAbits.TRMT);
		U1TXREG = '"';
	
		while(!U1STAbits.TRMT);
		U1TXREG = ' '; 	
	
		_U1RXIF=0;					// Clear UART RX Interrupt Flag
			
}
return 0;
}


void __attribute__((interrupt, no_auto_psv)) _T1Interrupt(void)
{
	IFS0bits.T1IF = 0;		// clear interrupt flag
	
	LATE ^= 0xF;			//Toggle LED's
}


