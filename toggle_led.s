;		DSPic flash a LED
;       .equ __30F2010, 1
        .include "p24fj64ga002.inc"

        .global toggle_led
.text
toggle_led:
		mov			#0xf000,W0		; Toggle PORTC<14> by XORing
		xor			LATB			; with 0x4000
		return
.end

