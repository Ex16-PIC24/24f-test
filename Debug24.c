/* Debug24.c
 *
 * Pretty silly little program to simply exercise the debugger
 * without running the sample program (which seems not to work
 * after rebuilding with MPLAB 7.60a).  Program mostly twiddles
 * some variables in the watch window.  To provide a little more
 * visual indication that something is happening, the LEDs
 * count up.
 *
 * JJMcD - 22 Jun 07
 *
 */

#include <p24fj64ga002.h>

int a;
unsigned int c;
char b;

int main( void )
{
	TRISB = 0x0fff;
	LATB = 0xf000;
	a = c = 0;
	b = ' ';
	
	while ( 1 )
	{
		a += 1511;
		if ( a > 0x3abe )
		{
			a = 1;
			c++;
		}
		b++;
		if ( b > 'Z' )
			b = '!';
		LATB = (c & 0xf000) ^ 0xf000;
	}
}
