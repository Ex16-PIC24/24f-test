/**********************************************************************************
* � 2007 Microchip Technology Inc.
*
* FileName:        Eg1_BlinkLED.c
* Dependencies:    p24fj64ga002.h
* Processor:       PIC24FJ64GA002
* Compiler:        MPLAB� C30 v3
*
* SOFTWARE LICENSE AGREEMENT:
* Microchip Technology Incorporated ("Microchip") retains all ownership and 
* intellectual property rights in the code accompanying this message and in all 
* derivatives hereto.  You may use this code, and any derivatives created by 
* any person or entity by or on your behalf, exclusively with Microchip,s 
* proprietary products.  Your acceptance and/or use of this code constitutes 
* agreement to the terms and conditions of this notice.
*
* CODE ACCOMPANYING THIS MESSAGE IS SUPPLIED BY MICROCHIP "AS IS".  NO 
* WARRANTIES, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED 
* TO, IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR A 
* PARTICULAR PURPOSE APPLY TO THIS CODE, ITS INTERACTION WITH MICROCHIP,S 
* PRODUCTS, COMBINATION WITH ANY OTHER PRODUCTS, OR USE IN ANY APPLICATION. 
*
* YOU ACKNOWLEDGE AND AGREE THAT, IN NO EVENT, SHALL MICROCHIP BE LIABLE, WHETHER 
* IN CONTRACT, WARRANTY, TORT (INCLUDING NEGLIGENCE OR BREACH OF STATUTORY DUTY), 
* STRICT LIABILITY, INDEMNITY, CONTRIBUTION, OR OTHERWISE, FOR ANY INDIRECT, SPECIAL, 
* PUNITIVE, EXEMPLARY, INCIDENTAL OR CONSEQUENTIAL LOSS, DAMAGE, FOR COST OR EXPENSE OF 
* ANY KIND WHATSOEVER RELATED TO THE CODE, HOWSOEVER CAUSED, EVEN IF MICROCHIP HAS BEEN 
* ADVISED OF THE POSSIBILITY OR THE DAMAGES ARE FORESEEABLE.  TO THE FULLEST EXTENT 
* ALLOWABLE BY LAW, MICROCHIP'S TOTAL LIABILITY ON ALL CLAIMS IN ANY WAY RELATED TO 
* THIS CODE, SHALL NOT EXCEED THE PRICE YOU PAID DIRECTLY TO MICROCHIP SPECIFICALLY TO 
* HAVE THIS CODE DEVELOPED.
*
* You agree that you are solely responsible for testing the code and 
* determining its suitability.  Microchip has no obligation to modify, test, 
* certify, or support the code.
*
*******************************************************************************************
*
*
* ADDITIONAL NOTES:  In this example program characters pressed on the keyboard are sent to
* the device via usb. The device replicates the character back to the PC enclosed in quotes. 
* Also this program blinks all four LEDs at 250ms by using timer 1. 
*
*******************************************************************************************/ 

#include "p24fj64ga002.h"

_CONFIG2(	IESO_OFF		// Two speed startup disabled
			&FNOSC_PRIPLL	// Primary oscillator (XT, HS, EC) w/ PLL
			&FCKSM_CSDCMD	// Clock switching and clock monitor disabled
			&OSCIOFNC_OFF	// OSCO or Fosc/2
			&IOL1WAY_ON		// Unlimited Writes To RP Registers
			&I2C1SEL_PRI	// Use Primary I2C1 pins
			&POSCMOD_XT)	// XT oscillator
_CONFIG1(	JTAGEN_OFF		// JTAG disabled
			&GCP_OFF		// Code Protect
			&GWRP_OFF		// Write Protect
			&BKBUG_OFF		// Background Debugger
			&COE_OFF		// Clip-on Emulation mode
			&ICS_PGx1		// ICD pins select
			&FWDTEN_ON		// Watchdog Timer
			&WINDIS_OFF		// Windowed WDT
			&FWPSA_PR128	// Watchdog prescaler
			&WDTPS_PS32768)	// Watchdog postscale


#define XTFREQ          7372800         		//On-board Crystal frequency
#define PLLMODE         2               		//On-chip PLL setting
#define FCY             XTFREQ*PLLMODE        //Instruction Cycle Frequency

#define BAUDRATE         9600       
#define BRGVAL          ((FCY/BAUDRATE)/16)-1 


int main(void)
{
	//int Count;

	CLKDIVbits.RCDIV = 0;

	RPINR18bits.U1RXR = 9;	// Make Pin RP9 U1RX
	RPOR4bits.RP8R = 3;		// Make Pin RP8 U1TX


	PADCFG1 = 0xFF;			// Make analog pins digital 

	LATB = 0x0;
	TRISB = 0x0300;			// Configure LED pins as output

	TMR1 = 0;				// Clear timer 1
	PR1 = 0x3D09;			// Interrupt every 250ms
	IFS0bits.T1IF = 0;		// Clear interrupt flag
	//IEC0bits.T1IE = 1;		// Set interrupt enable bit
	T1CON = 0x8030;			// Fosc/4, 1:256 prescale, start TMR1


	TRISB = 0x0000;
	U1BRG  = BRGVAL;
	U1MODE = 0x8000; 		// Reset UART to 8-n-1, alt pins, and enable 
	U1STA  = 0x0440; 		// Reset status register and enable TX & RX


	_U1RXIF=0;					// Clear UART RX Interrupt Flag
	

	while(1)
	{
		int a,b;
		
		for ( b=0; b<10; b++ )
			for ( a=0; a<30000; a++ )
				;
		//LATB ^= 0xF000;			//Toggle LED's
		LATB = 0xc000;
		
		for ( b=0; b<10; b++ )
			for ( a=0; a<30000; a++ )
				;
		LATB = 0x3000;
		
		for ( b=0; b<10; b++ )
			for ( a=0; a<30000; a++ )
				;
		LATB = 0x5000;
		
		for ( b=0; b<10; b++ )
			for ( a=0; a<30000; a++ )
				;
		LATB = 0xa000;

			
	}
return 0;
}



