;		DSPic flash a LED
;       .equ __30F2010, 1
        .include "p24fj64ga002.inc"

        .global port_init

.text								;Start of Code section
;..............................................................................
;Subroutine: Initialization of PORTC
;..............................................................................
port_init:
		MOV			#0x0000,W2		; PORTC only has <13:15>
		MOV			W2,TRISB
		RETURN

.end
